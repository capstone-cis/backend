package com.sd3.capstone.dtos;

import com.sd3.capstone.entities.UserRole;

public record UserDTO(String name, String login, String password, UserRole role) {

}