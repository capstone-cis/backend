package com.sd3.capstone.Services;

import com.sd3.capstone.dtos.UserDTO;
import com.sd3.capstone.entities.User;
import com.sd3.capstone.repositories.UserRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServices {
    @Autowired
    private UserRepositories myRepository;

    public User saveUser(UserDTO userDTO){
        var user = new User(userDTO);
        return this.myRepository.save(user);
    }

    public List<User> getUser(){
        return myRepository.findAll();
    }

    public void deleteUser(String id){
        myRepository.deleteById(id);
    }

    public User updateUser(String id, UserDTO userDTO){
        User user = myRepository.findById(id).orElseThrow();

        if(userDTO.login() != null) user.setLogin(userDTO.login());

        if(userDTO.name() != null) user.setName(userDTO.name());

        if(userDTO.password() != null) user.setPassword(userDTO.password());

        if (userDTO.role() != null) user.setRole(userDTO.role());


        return myRepository.save(user);
    }


    public User getUserByLogin(String login){
        return myRepository.findByLogin(login);
    }


}