package com.sd3.capstone.Services;

import com.sd3.capstone.dtos.UserDTO;
import com.sd3.capstone.entities.User;
import com.sd3.capstone.entities.UserRole;
import com.sd3.capstone.repositories.UserRepositories;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@DataJpaTest
@ActiveProfiles("test")
class UserServicesTest {


    @Autowired
    EntityManager entityManager;

    @Autowired
    @Mock
    private UserRepositories userRepositories;

    @InjectMocks
    private UserServices userServices;

    @BeforeEach
    void setUp() {
        // Inicializa os mocks antes de cada teste
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void saveUser() {

        UserDTO userDTO = new UserDTO("username", "password", "John Doe", UserRole.ADMIN);
        User user = new User(userDTO);
        when(userRepositories.save(any(User.class))).thenReturn(user);

        User savedUser = userServices.saveUser(userDTO);

        assertNotNull(savedUser);
        assertEquals(userDTO.login(), savedUser.getLogin());
        assertEquals(userDTO.password(), savedUser.getPassword());
        assertEquals(userDTO.name(), savedUser.getName());
        verify(userRepositories, times(1)).save(any(User.class));
    }

    @Test
    void getUser() {

        when(userRepositories.findAll()).thenReturn(Collections.singletonList(new User()));

        List<User> users = userServices.getUser();

        assertNotNull(users);
        assertFalse(users.isEmpty());
        verify(userRepositories, times(1)).findAll();
    }

    @Test
    void deleteUser() {

        String userId = "1";

        userServices.deleteUser(userId);

        verify(userRepositories, times(1)).deleteById(userId);
    }

    @Test
    void updateUser() {

        String userId = "1";
        UserDTO userDTO = new UserDTO("newUsername", "newPassword", "New Name", UserRole.ADMIN);
        User existingUser = new User(userDTO);
        when(userRepositories.findById(userId)).thenReturn(Optional.of(existingUser));
        when(userRepositories.save(any(User.class))).thenReturn(existingUser);

        User updatedUser = userServices.updateUser(userId, userDTO);

        assertNotNull(updatedUser);
        assertEquals(userDTO.login(), updatedUser.getLogin());
        assertEquals(userDTO.password(), updatedUser.getPassword());
        assertEquals(userDTO.name(), updatedUser.getName());
        verify(userRepositories, times(1)).findById(userId);
        verify(userRepositories, times(1)).save(existingUser);
    }

    @Test
    void getUserByLogin() {

        String login = "username";
        User user = new User(new UserDTO("NameTest", login, "John Doe", UserRole.ADMIN));
        when(userRepositories.findByLogin(login)).thenReturn(user);

        User foundUser = userServices.getUserByLogin(login);

        assertNotNull(foundUser);
        assertEquals(login, foundUser.getLogin());
        verify(userRepositories, times(1)).findByLogin(login);
    }
}
