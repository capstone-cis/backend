package com.sd3.capstone.repositories;

import com.sd3.capstone.dtos.UserDTO;
import com.sd3.capstone.entities.User;
import com.sd3.capstone.entities.UserRole;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import static org.assertj.core.api.Assertions.*;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ActiveProfiles("test")
class UserRepositoriesTest {
    @Autowired
    UserRepositories userRepositories;

    @Autowired
    EntityManager entityManager;

    @Test
    @DisplayName("Should get User sucessfully from DB")
    void findByLoginSucess() {
        String loginTest = "loginTest";
        UserDTO data = new UserDTO("FelipeTest", loginTest, "passTest", UserRole.ADMIN);
        this.createuser(data);

      User result = this.userRepositories.findByLogin(loginTest);

    }

    private User createuser(UserDTO data){
        User newUser = new User(data);
        this.entityManager.persist(newUser);
        return newUser;
    }
}