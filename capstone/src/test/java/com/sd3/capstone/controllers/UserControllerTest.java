package com.sd3.capstone.controllers;

import com.sd3.capstone.Services.UserServices;
import com.sd3.capstone.dtos.UserDTO;
import com.sd3.capstone.entities.User;
import com.sd3.capstone.entities.UserRole;
import com.sd3.capstone.repositories.UserRepositories;
import jakarta.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@DataJpaTest
@ActiveProfiles("test")
class UserControllerTest {

    @Autowired
    UserRepositories userRepositories;

    @Autowired
    EntityManager entityManager;
    @Mock
    private UserServices userService;

    @InjectMocks
    private UserControllers userController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void saveUser() {
        UserDTO userDTO = new UserDTO("danilo", "danilo123", "danilo1", UserRole.USER);
        User user = new User();
        when(userService.saveUser(any(UserDTO.class))).thenReturn(user);

        ResponseEntity<User> responseEntity = userController.saveUser(userDTO);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(user, responseEntity.getBody());
        verify(userService, times(1)).saveUser(any(UserDTO.class));
    }

    @Test
    void getUser() {
        List<User> userList = Collections.singletonList(new User());
        when(userService.getUser()).thenReturn(userList);

        ResponseEntity<List<User>> responseEntity = userController.getUser();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(userList, responseEntity.getBody());
        verify(userService, times(1)).getUser();
    }

    @Test
    void deleteUser() {
        String id = "1";
        doNothing().when(userService).deleteUser(id);

        ResponseEntity<User> responseEntity = userController.deleteUser(id);

        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
        verify(userService, times(1)).deleteUser(id);
    }

    private User createuser(UserDTO data){
        User newUser = new User(data);
        this.entityManager.persist(newUser);
        return newUser;
    }

    @Test
    void updateUser() {
        String loginTest = "loginTest";
        UserDTO data = new UserDTO("DaniloTest", loginTest, "danilo", UserRole.ADMIN);
        this.createuser(data);
        String id = "1";
        UserDTO userDTO = new UserDTO("danilo2", "danilo123", "danilo1", UserRole.ADMIN);
        User user = new User();
        when(userService.updateUser(eq(id), any(UserDTO.class))).thenReturn(user);

        ResponseEntity<User> responseEntity = userController.updateUser(id, userDTO);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(user, responseEntity.getBody());
        verify(userService, times(1)).updateUser(eq(id), any(UserDTO.class));
    }

    @Test
    void getUserByLogin() {
        String loginTest = "loginTest";
        UserDTO data = new UserDTO("DaniloTest", loginTest, "danilo", UserRole.ADMIN);
        this.createuser(data);

        User user = new User();

        when(userService.getUserByLogin(loginTest)).thenReturn(user);

        ResponseEntity<User> responseEntity = userController.getUserByLogin(loginTest);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(user, responseEntity.getBody());
        verify(userService, times(1)).getUserByLogin(loginTest);
    }
}
